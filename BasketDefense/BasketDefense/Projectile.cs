﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BasketDefense
{
    class Projectile
    {
        public Vector2 pos;
        public Rectangle coll;
        public Single dir;
        public Int16 type;
        public Texture2D tex;
        public Single power;
        public Single speed;
        public Color color;

        public Projectile(Vector2 pos, Single dir, Int16 type)
        {
            this.pos = pos;
            this.dir = dir;
            this.type = type;
            switch (this.type)
            {
                case 0:
                    tex = Game1.texError;
                    speed = 5.0f;
                    power = 5.0f;
                    break;
                case 1:
                    tex = Game1.texProj1;
                    power = 0.3f;
                    speed = 5.0f;
                    color = new Color(255,255,0);
                    break;
                case 2:
                    tex = Game1.texProj1;
                    power = 0.0f;
                    speed = 8.0f;
                    color = new Color(0,128,255);
                    break;
            }
            
            coll = new Rectangle((int)this.pos.X, (int)this.pos.Y, tex.Width, tex.Height);
        }

        public void Update()
        {
            coll = new Rectangle((int)this.pos.X, (int)this.pos.Y, tex.Width, tex.Height);
            //this.pos.X += 5;
            this.pos.X += (float)(speed * Math.Cos(dir));
            this.pos.Y += (float)(speed * Math.Sin(dir));

            for (int i = 0; i < StateMap1.enemyRoster.Count; i++)
            {
                switch (type)
                {
                    case 2:
                        if (coll.Intersects(StateMap1.enemyRoster[i].bounds))
                        {
                            StateMap1.enemyRoster[i].status[0] = 1;
                            StateMap1.enemyRoster[i].statusTimer[0] = 500;

                            if (StateMap1.enemyRoster[i].type == 3)
                            {
                                StateMap1.enemyRoster[i].health -= .1f; // Make fire ants weak to water towers.
                            }
                        }
                        break;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(tex, coll, color);
        }
    }
}
