﻿namespace BasketDefense
{
    partial class Cheats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSetHealth = new System.Windows.Forms.Button();
            this.txtInputHealth = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtInputFunds = new System.Windows.Forms.TextBox();
            this.btnSetFunds = new System.Windows.Forms.Button();
            this.chkOverlays = new System.Windows.Forms.CheckBox();
            this.lblMouseInfo = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txtInputLevel = new System.Windows.Forms.TextBox();
            this.btnSetLevel = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.chkPause = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnSetHealth
            // 
            this.btnSetHealth.Location = new System.Drawing.Point(205, 13);
            this.btnSetHealth.Name = "btnSetHealth";
            this.btnSetHealth.Size = new System.Drawing.Size(75, 23);
            this.btnSetHealth.TabIndex = 0;
            this.btnSetHealth.Text = "Set";
            this.btnSetHealth.UseVisualStyleBackColor = true;
            this.btnSetHealth.Click += new System.EventHandler(this.btnSetHealth_Click);
            // 
            // txtInputHealth
            // 
            this.txtInputHealth.Location = new System.Drawing.Point(57, 16);
            this.txtInputHealth.Name = "txtInputHealth";
            this.txtInputHealth.Size = new System.Drawing.Size(142, 20);
            this.txtInputHealth.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Health:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Funds:";
            // 
            // txtInputFunds
            // 
            this.txtInputFunds.Location = new System.Drawing.Point(57, 42);
            this.txtInputFunds.Name = "txtInputFunds";
            this.txtInputFunds.Size = new System.Drawing.Size(142, 20);
            this.txtInputFunds.TabIndex = 4;
            // 
            // btnSetFunds
            // 
            this.btnSetFunds.Location = new System.Drawing.Point(205, 39);
            this.btnSetFunds.Name = "btnSetFunds";
            this.btnSetFunds.Size = new System.Drawing.Size(75, 23);
            this.btnSetFunds.TabIndex = 3;
            this.btnSetFunds.Text = "Set";
            this.btnSetFunds.UseVisualStyleBackColor = true;
            this.btnSetFunds.Click += new System.EventHandler(this.btnSetFunds_Click);
            // 
            // chkOverlays
            // 
            this.chkOverlays.AutoSize = true;
            this.chkOverlays.Location = new System.Drawing.Point(35, 103);
            this.chkOverlays.Name = "chkOverlays";
            this.chkOverlays.Size = new System.Drawing.Size(67, 17);
            this.chkOverlays.TabIndex = 6;
            this.chkOverlays.Text = "Overlays";
            this.chkOverlays.UseVisualStyleBackColor = true;
            this.chkOverlays.CheckedChanged += new System.EventHandler(this.chkOverlays_CheckedChanged);
            // 
            // lblMouseInfo
            // 
            this.lblMouseInfo.Location = new System.Drawing.Point(108, 104);
            this.lblMouseInfo.Name = "lblMouseInfo";
            this.lblMouseInfo.Size = new System.Drawing.Size(184, 16);
            this.lblMouseInfo.TabIndex = 7;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Level:";
            // 
            // txtInputLevel
            // 
            this.txtInputLevel.Location = new System.Drawing.Point(57, 67);
            this.txtInputLevel.Name = "txtInputLevel";
            this.txtInputLevel.Size = new System.Drawing.Size(142, 20);
            this.txtInputLevel.TabIndex = 9;
            // 
            // btnSetLevel
            // 
            this.btnSetLevel.Location = new System.Drawing.Point(205, 64);
            this.btnSetLevel.Name = "btnSetLevel";
            this.btnSetLevel.Size = new System.Drawing.Size(75, 23);
            this.btnSetLevel.TabIndex = 8;
            this.btnSetLevel.Text = "Set";
            this.btnSetLevel.UseVisualStyleBackColor = true;
            this.btnSetLevel.Click += new System.EventHandler(this.btnSetLevel_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(205, 123);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 11;
            this.btnExit.Text = "Exit Game";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // chkPause
            // 
            this.chkPause.AutoSize = true;
            this.chkPause.Location = new System.Drawing.Point(35, 123);
            this.chkPause.Name = "chkPause";
            this.chkPause.Size = new System.Drawing.Size(56, 17);
            this.chkPause.TabIndex = 12;
            this.chkPause.Text = "Pause";
            this.chkPause.UseVisualStyleBackColor = true;
            // 
            // Cheats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 154);
            this.Controls.Add(this.chkPause);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtInputLevel);
            this.Controls.Add(this.btnSetLevel);
            this.Controls.Add(this.lblMouseInfo);
            this.Controls.Add(this.chkOverlays);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtInputFunds);
            this.Controls.Add(this.btnSetFunds);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtInputHealth);
            this.Controls.Add(this.btnSetHealth);
            this.Name = "Cheats";
            this.Text = "Cheats";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSetHealth;
        private System.Windows.Forms.TextBox txtInputHealth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtInputFunds;
        private System.Windows.Forms.Button btnSetFunds;
        private System.Windows.Forms.CheckBox chkOverlays;
        private System.Windows.Forms.Label lblMouseInfo;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtInputLevel;
        private System.Windows.Forms.Button btnSetLevel;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.CheckBox chkPause;
    }
}