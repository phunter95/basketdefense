//-----------------------------------------//
// Basket Defense | Program.cs-------------//
// By: Phillip Hunter----------------------//
// Desc: Main program file.----------------//
//-----------------------------------------//

using System;

namespace BasketDefense
{
#if WINDOWS || XBOX
    static class Program
    {
        public static Game1 game = new Game1();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (game)
            {
                game.Run();
            }
        }
    }
#endif
}
