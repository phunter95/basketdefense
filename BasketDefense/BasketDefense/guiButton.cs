﻿//--------------------------------------------------------//
// Basket Defense | guiButton.cs--------------------------//
// By: Phillip Hunter-------------------------------------//
// Desc: Button class used for all interface buttons.-----//
//--------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BasketDefense
{
    class guiButton
    {
        #region Button
        public Int16 type; //1 = Main menu, 2 = In game start round, 3 = Tower selection;
        private String text;
        private Rectangle bounds;
        private Keys key;

        public Boolean active = true;
        private Vector2 textPos;

        public Int16 buttonState = 0; //0 = norm, 1 = hover, 2 = press;
        private Boolean pressed = false;
        private Boolean go = false;

        public Int16 tower;
        private Color towerButtonColor = Color.White;
        #endregion Button

        #region Textures
        #region Main
        private Texture2D texCurrent;
        private Texture2D texNorm;
        private Texture2D texHover;
        private Texture2D texPress;
        #endregion Main
        #region Title Button
        private Texture2D texTitleButtonNorm;
        private Texture2D texTitleButtonHover;
        private Texture2D texTitleButtonPress;
        #endregion Title Button

        #region Start Button
        private Texture2D texStartButtonNorm;
        private Texture2D texStartButtonHover;
        private Texture2D texStartButtonPress;
        #endregion Start Button

        #region Tower Button
        private Texture2D texTowerButton;
        private Texture2D texTowerButtonTurret;
        #endregion Tower Button
        #endregion Textures

        #region Fonts
        private SpriteFont fntCurrent;
        private SpriteFont fntTitle;
        private SpriteFont fntStart;
        #endregion Fonts

        #region Colors
        #region Main
        private Color colCurrent;
        private Color colNorm;
        private Color colHover;
        private Color colPress;
        #endregion Main

        #region Title Button
        private Color colTitleButtonNorm = new Color(0, 0, 255);
        private Color colTitleButtonHover = new Color(255, 255, 0);
        private Color colTitleButtonPress = new Color(200, 200, 0);
        #endregion Title Button

        #region Start Button
        private Color colStartButtonNorm = new Color(0, 30, 0);
        private Color colStartButtonHover = new Color(0, 30, 0);
        private Color colStartButtonPress = new Color(128, 255, 128);
        #endregion
        #endregion

        /// <summary>
        /// Button Constructor
        /// </summary>
        /// <param name="type"1: Title Screen, 2: In Game Start Round;></param>
        /// <param name="text">Text to show in the button.</param>
        /// <param name="pos">XY coords of button.</param>
        public guiButton(Int16 type, String text, Vector2 pos)
        {
            this.type = type;
            this.text = text;
            InitType(type, pos);
        }

        /// <summary>
        /// Button Constructor with Keybind
        /// </summary>
        /// <param name="type"1: Title Screen, 2: In Game Start Round;></param>
        /// <param name="text">Text to show in the button.</param>
        /// <param name="key">Keyboard shortcut to button.</param>
        /// <param name="pos">XY coords of button</param>
        public guiButton(Int16 type, String text, Vector2 pos, Keys key)
        {
            this.type = type;
            this.text = text;
            this.key = key;
            InitType(type, pos);
        }

        /// <summary>
        /// Button Constructor with Keybind and Tower
        /// </summary>
        /// <param name="type"1: Title Screen, 2: In Game Start Round;></param>
        /// <param name="text">Text to show in the button.</param>
        /// <param name="key">Keyboard shortcut to button.</param>
        /// <param name="tower">ID of the tower to make this button switch to.</param>
        /// <param name="pos">XY coords of button</param>
        public guiButton(Int16 type, String text, Vector2 pos, Keys key, Int16 tower)
        {
            this.type = type;
            this.text = text;
            this.key = key;
            this.tower = tower;
            InitType(type, pos);
        }

        public void LoadContent()
        {
            // May be redundant.
            texCurrent = Game1.texError;
            texNorm = Game1.texError;
            texHover = Game1.texError;
            texPress = Game1.texError;

            fntCurrent = Program.game.LoadFont("fntError");
            //

            // Only load normal, hover, and press textures based on type/tower (if applicable).
            switch (type)
            {
                case 1:
                    texTitleButtonNorm = Program.game.LoadTexture("gui/buttons/title/TitleButtonNorm");
                    texTitleButtonHover = Program.game.LoadTexture("gui/buttons/title/TitleButtonHover");
                    texTitleButtonPress = Program.game.LoadTexture("gui/buttons/title/TitleButtonPressed");
                    fntTitle = Program.game.LoadFont("fntTitleButton");
                    break;
                case 2:
                    texStartButtonNorm = Program.game.LoadTexture("gui/buttons/start/StartButtonNorm");
                    texStartButtonHover = Program.game.LoadTexture("gui/buttons/start/StartButtonHover");
                    texStartButtonPress = Program.game.LoadTexture("gui/buttons/start/StartButtonPressed");
                    fntStart = Program.game.LoadFont("fntGameMenu1");
                    break;
                case 3:
                    switch (tower)
                    {
                        case 0:
                            texTowerButton = Game1.texTestTower;
                            texTowerButtonTurret = Game1.texTestTowerTurret;
                            break;
                        case 1:
                            texTowerButton = Game1.texBasicAntShooter;
                            texTowerButtonTurret = Game1.texBasicAntShooterTurret;
                            break;
                        case 2:
                            texTowerButton = Game1.texWaterTower;
                            texTowerButtonTurret = Game1.texWaterTowerTurret;
                            break;
                    }
                    break;
            }
        }

        public void UnloadContent()
        {
            texCurrent.Dispose();
            texNorm.Dispose();
            texHover.Dispose();
            texPress.Dispose();

            texTitleButtonNorm.Dispose();
            texTitleButtonHover.Dispose();
            texTitleButtonPress.Dispose();
        }

        public void Update()
        {
            // Set the normal, hover, and press textures depending on their various types.
            switch (type)
            {
                case 1:
                    texNorm = texTitleButtonNorm;
                    texHover = texTitleButtonHover;
                    texPress = texTitleButtonPress;

                    colNorm = colTitleButtonNorm;
                    colHover = colTitleButtonHover;
                    colPress = colTitleButtonPress;

                    fntCurrent = fntTitle;
                    break;

                case 2:
                    texNorm = texStartButtonNorm;
                    texHover = texStartButtonHover;
                    texPress = texStartButtonPress;

                    colNorm = colStartButtonNorm;
                    colHover = colStartButtonHover;
                    colPress = colStartButtonPress;

                    fntCurrent = fntStart;
                    break;

                case 3:
                    texCurrent = texTowerButton;
                    break;
            }

            if (this.type != 3)
            {
                // Switch the current texture and color based on the current button state.
                // if the button type is not a tower selection button.
                switch (buttonState)
                {
                    case 0:
                        texCurrent = texNorm;
                        colCurrent = colNorm;
                        break;
                    case 1:
                        texCurrent = texHover;
                        colCurrent = colHover;
                        break;
                    case 2:
                        texCurrent = texPress;
                        colCurrent = colPress;
                        break;
                }
            }
            else
            {
                // If type is a tower selection button, then change the button color only, based on the state.
                switch (buttonState)
                {
                    case 0:
                        towerButtonColor = new Color(255, 255, 255);
                        break;
                    case 1:
                        towerButtonColor = new Color(0, 255, 255);
                        break;
                    case 2:
                        towerButtonColor = new Color(0, 150, 150);
                        break;
                }
            }
        }

        public void Input(MouseState thaMouse)
        {
            if (active)
            {
                if (bounds.Contains(new Point(thaMouse.X, thaMouse.Y)))
                {
                    buttonState = 1;
                    if (thaMouse.LeftButton == ButtonState.Pressed)
                    {
                        buttonState = 2;
                    }
                    if (buttonState == 2)
                    {
                        pressed = true;
                    }
                }
                else
                {
                    buttonState = 0;
                    pressed = false;
                    go = false;
                }

                if ((pressed == true) && (thaMouse.LeftButton == ButtonState.Released) && (bounds.Contains(new Point(thaMouse.X, thaMouse.Y))))
                {
                    Game1.audMenuClick.Play(Game1.GUI_VOLUME, 0.0f, 0.0f);
                    go = true;
                }
            }
        }

        public void Input(MouseState thaMouse, KeyboardState thaKeyboard, KeyboardState thaOldKeyboard)
        {
            if (active)
            {
                if (bounds.Contains(new Point(thaMouse.X, thaMouse.Y)))
                {
                    buttonState = 1;
                    if (thaMouse.LeftButton == ButtonState.Pressed)
                    {
                        buttonState = 2;
                    }
                    if (buttonState == 2)
                    {
                        pressed = true;
                    }
                }
                else
                {
                    buttonState = 0;
                    pressed = false;
                    go = false;
                }

                if (((pressed == true) && (thaMouse.LeftButton == ButtonState.Released) && (bounds.Contains(new Point(thaMouse.X, thaMouse.Y)))) || InputManager.IsKeyPressed(key, thaKeyboard, thaOldKeyboard))
                {
                    go = true;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (active)
            {
                if (type == 1 || type == 2)
                {
                    spriteBatch.Draw(texCurrent, bounds, Color.White);
                    spriteBatch.DrawString(fntCurrent, text, textPos, colCurrent);
                }
                if (type == 3)
                {
                    spriteBatch.Draw(texCurrent, bounds, towerButtonColor);
                    spriteBatch.Draw(texTowerButtonTurret, bounds, towerButtonColor);
                }
            }
            else
            {
                spriteBatch.Draw(texCurrent, bounds, new Color(95, 95, 95));
                if (this.type != 3)
                {
                    spriteBatch.DrawString(fntCurrent, text, textPos, new Color(colCurrent.R - 160, colCurrent.G - 160, colCurrent.B - 160));
                }
            }

        }

        /// <summary>
        /// Sets up properties for each type 
        /// </summary>
        private void InitType(Int16 type, Vector2 pos)
        {
            switch (type)
            {
                case 1:
                    bounds = new Rectangle((int)pos.X, (int)pos.Y, 145, 40);
                    textPos = new Vector2(bounds.X + 10, bounds.Y);
                    break;
                case 2:
                    bounds = new Rectangle((int)pos.X, (int)pos.Y, 68, 27);
                    textPos = new Vector2(bounds.X, bounds.Y - 3);
                    break;
                case 3:
                    bounds = new Rectangle((int)pos.X, (int)pos.Y, 32, 32);
                    textPos = new Vector2(bounds.X, bounds.Y - 3);
                    break;
            }
        }

        public bool isButtonClicked()
        {
            return go;
        }

        public void Reset()
        {
            this.go = false;
            this.pressed = false;
        }
    }
}

