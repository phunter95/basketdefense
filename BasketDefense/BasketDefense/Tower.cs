﻿//---------------------------------------------------------------//
// Basket Defense | Tower.cs-------------------------------------//
// By: Phillip Hunter--------------------------------------------//
// Desc: Defensive objects that target Enemies.------------------//
//---------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using C3.XNA;

namespace BasketDefense
{
    class Tower
    {
        private Int16 type;
        public Vector2 pos;
        public static Int32[] costs = new Int32[] { 42, 120, 150 };

        private Int16 power;
        private Single speed;
        public Int32 radius = 32;
        public Int32 rate = 0; // Lower the faster. Utilizes tickCount.

        public Boolean alive = true;
        private Single rotation = 0.0f; //TODO Make it rotate slowly to enemy.
        private Single idleRotation = 0.0f;
        private Vector2 direction;
        private Rectangle bounds;
        private Rectangle coll;

        private Texture2D baseTexture;
        private Texture2D turretTexture;
        private Boolean doShowRadius = false;

        private List<Enemy> targetableRoster = new List<Enemy>();
        private Enemy target;
        private Single range;

        public Tower(Int16 type, Int32 x, Int32 y)
        {
            this.type = type;
            this.pos.X = x;
            this.pos.Y = y;

            // Set the tower properties based on the type.
            switch (type)
            {
                case 0:
                    baseTexture = Game1.texTestTower;
                    turretTexture = Game1.texTestTowerTurret;
                    range = 100;
                    rate = 20;
                    break;
                case 1:
                    baseTexture = Game1.texBasicAntShooter;
                    turretTexture = Game1.texBasicAntShooterTurret;
                    range = 150;
                    rate = 30;
                    break;
                case 2:
                    baseTexture = Game1.texWaterTower;
                    turretTexture = Game1.texWaterTowerTurret;
                    range = 120;
                    rate = 3;
                    break;
            }

            this.bounds = new Rectangle(x, y, radius, radius);
            this.coll = new Rectangle((int)this.pos.X - (radius / 2), (int)this.pos.Y - (radius / 2), radius, radius);
        }

        public void Update()
        {
            #region Idle Animation
            // Slowly rotates the turret. Used when idle.
            idleRotation += .01f;
            if (idleRotation >= 2 * Math.PI)
            {
                idleRotation = 0;
            }
            #endregion Idle Animation

            #region Targeting System
            if (StateMap1.enemyRoster.Count > 0)
            {
                // Clear the list of targetable ants.
                targetableRoster.Clear();
                // Cycle each Enemy and add it to the targetable roster if it is within range.
                for (int i = 0; i < StateMap1.enemyRoster.Count; i++)
                {
                    if (Vector2.Distance(this.pos, StateMap1.enemyRoster[i].pos) < range)
                    {
                        targetableRoster.Add(StateMap1.enemyRoster[i]);
                    }
                }

                // Target the first member of hte targetable roster if it is not empty.
                if (targetableRoster.Count > 0)
                {
                    target = targetableRoster[0];
                }

                //If the target is not null.
                if (target != null)
                {
                    // If the target is in range.
                    if (Vector2.Distance(this.pos, this.target.pos) < range)
                    {
                        direction = target.pos - this.pos;
                        //Face the target.
                        rotation = (float)Math.Atan2(direction.Y, direction.X);

                        // Shoot the target.
                        if ((Game1.tickCount % rate == 0) && (StateMap1.SubState == 1))
                        {
                            switch (type)
                            {
                                case 0:
                                    StateMap1.projectileRoster.Add(new Projectile(this.pos, rotation, 1));
                                    Game1.audTowerFire.Play(Game1.SFX_VOLUME, 0.0f, 0.0f);
                                    break;
                                case 1:
                                    StateMap1.projectileRoster.Add(new Projectile(this.pos, rotation, 1));
                                    Game1.audTowerFire.Play(Game1.SFX_VOLUME, 0.0f, 0.0f);
                                    break;
                                case 2:
                                    StateMap1.projectileRoster.Add(new Projectile(this.pos, rotation, 2));
                                    Game1.audTowerFireWater.Play(Game1.SFX_VOLUME, 0.0f, 0.0f);
                                    break;
                            }

                        }
                    }
                }

            #endregion Targeting System

                // Show the radius if the collision box contains the point of the mouse cursor.
                if (coll.Contains(new Point(StateMap1.mouseX, StateMap1.mouseY)))
                {
                    doShowRadius = true;
                }
                else
                {
                    doShowRadius = false;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //Draw the tower base.
            spriteBatch.Draw(baseTexture, bounds, null, Color.White, 0, new Vector2(radius / 2, radius / 2), SpriteEffects.None, 0);
            //Draw the tower turret.
            spriteBatch.Draw(turretTexture, bounds, null, Color.White, rotation, new Vector2(radius / 2, radius / 2), SpriteEffects.None, 0);

            //Draw the radius if doShowRadius is true.
            if (doShowRadius)
            {
                for (int i = 0; i < StateMap1.towerRoster.Count; i++)
                {
                    StateMap1.towerRoster[i].doShowRadius = false;
                }
                Primitives2D.DrawCircle(spriteBatch, new Vector2(this.pos.X + (radius / 8), this.pos.Y + (radius / 8)), range, 32, Color.White);
            }
        }
    }
}
