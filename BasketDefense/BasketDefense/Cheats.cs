﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace BasketDefense
{
    public partial class Cheats : Form
    {
        public Cheats()
        {
            InitializeComponent();
        }

        private void btnSetHealth_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse(txtInputHealth.Text, out StateMap1.gameLife) == false)
            {
                MessageBox.Show("Please enter a valid number.");
            }
        }

        private void btnSetFunds_Click(object sender, EventArgs e)
        {
            if (Int64.TryParse(txtInputFunds.Text, out StateMap1.funds) == false)
            {
                MessageBox.Show("Please enter a valid number.");
            }
        }

        private void btnSetLevel_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse(txtInputLevel.Text, out StateMap1.currentRound) == false)
            {
                MessageBox.Show("Please enter a valid number.");
            }
        }

        private void chkOverlays_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOverlays.Checked == true)
            {
                CVar.overlays = 1;
            }
            else
            {
                CVar.overlays = 0;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblMouseInfo.Text = "Mouse Position: (" + Game1.thaMouse.X + "," + Game1.thaMouse.Y + ")";
            if(chkPause.Checked == true)
            {
                Thread.Sleep(200);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Game1.exitFlag = true;
        }
    }
}
