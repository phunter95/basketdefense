﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace BasketDefense
{
    class StateIntroScreen : State
    {
        public static Boolean useFallbackSystem = false;
        private Texture2D texIntroBackground;
        private Int32 screenAlpha = 0;
        private Int32 screenAlphaSpeed = 2;
        private Boolean screenAlphaUp = true;
        private Int64 timer;

        private VideoPlayer videoPlayer;
        private Texture2D introVideoTexture;
        private Boolean videoPlayed = false;

        private KeyboardState thaOldKeyboard = Keyboard.GetState();
        private MouseState thaOldMouse = Mouse.GetState();

        public override void Init()
        {
        }
        public override void LoadContent()
        {
            texIntroBackground = Program.game.LoadTexture("gui/introScreen/introScreen");

            if (useFallbackSystem == false)
            {
                videoPlayer = new VideoPlayer();
            }
        }
        public override void UnloadContent()
        {
        }
        public override void Update()
        {
            #region Fallback
            if (useFallbackSystem == true)
            {
                timer++;
                switch (screenAlphaUp)
                {
                    case true:
                        screenAlpha += screenAlphaSpeed;
                        break;
                    case false:
                        screenAlpha -= screenAlphaSpeed;
                        break;
                }

                if (screenAlpha >= 255)
                {
                    screenAlphaUp = false;
                }

                if (timer >= 257)
                {
                    Program.game.sm.SetState(new StateMenu());
                }
            }
            #endregion Fallback

            if (useFallbackSystem == false)
            {
                if (videoPlayer.State == MediaState.Stopped && videoPlayed == false)
                {
                    videoPlayer.Play(Game1.introVideo);
                    videoPlayed = true;
                }

                if ((videoPlayed == true) && (videoPlayer.State == MediaState.Stopped))
                {
                    Program.game.sm.SetState(new StateMenu());
                }
            }
        }
        public override void Input(KeyboardState thaKeyboard, MouseState thaMouse)
        {

            if ((InputManager.IsKeyPressed(Keys.Space, thaKeyboard, thaOldKeyboard)) || (InputManager.IsMouseLeftPressed(thaMouse, thaOldMouse)))
            {
                if (useFallbackSystem == false)
                {
                    videoPlayer.Stop();
                }
                Program.game.sm.SetState(new StateMenu());
            }

            thaOldKeyboard = thaKeyboard;
            thaOldMouse = thaMouse;
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            Game1.graphicsDeviceClearColor = Color.Black;
            #region Fallback
            if (useFallbackSystem == true)
            {
                spriteBatch.Draw(texIntroBackground, new Microsoft.Xna.Framework.Rectangle(0, 0, Game1.GAME_WIDTH, Game1.GAME_HEIGHT), new Color(255, 255, 255, screenAlpha));
            }
            #endregion Fallback
            if (useFallbackSystem == false)
            {
                if (videoPlayer.State != MediaState.Stopped)
                {
                    introVideoTexture = videoPlayer.GetTexture();
                }
                spriteBatch.Draw(introVideoTexture, new Microsoft.Xna.Framework.Rectangle(0, 0, Game1.GAME_WIDTH, Game1.GAME_HEIGHT), new Color(255, 255, 255));
            }
        }
        public override void DeInit()
        {
        }
    }
}
