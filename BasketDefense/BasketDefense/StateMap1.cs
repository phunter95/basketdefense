﻿//---------------------------------------------------------------//
// Basket Defense | StateMap1.cs---------------------------------//
// By: Phillip Hunter--------------------------------------------//
// Desc: State for Map1.-----------------------------------------//
//---------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BasketDefense
{
    class StateMap1 : State
    {
        //Variables
        #region Game
        Random rGen = new Random();

        public static Rectangle rectBasket = new Rectangle(514, 514, 86, 86); // Location of the target.
        public static Rectangle spawnPoint = new Rectangle(0, 26, 37, 23); // Location of the spawn point of the enemies.

        public static Int32 gameLife; // Number of lives possessed by the player.
        public static Int64 funds; // Amount of money possessed by the player.
        public static Int32 currentRound = 1; // Current round of the game.
        private Int32 spawnRound; // Not the current round. Used for spawning.
        private Int64 time = 0; // Time used for the enemy spawn system.

        public static Int16 SubState = 0; // 0 = Building Selection, 1 = Active simulation, 2 = Death screen;

        public static Int64 numOfEnemies = 0;
        //private Enemy finalAnt; // Final ant spawned, used to detect when round is over.
        //private Boolean finalAntSpawned = false; // Is the final ant has spawned?

        public bool isMouseInValidArea = true; // Is the mouse in a valid area to place a tower?

        private String infoText = ""; //The current informational text about the tower button.
        private String[] infoTexts; // Array of possible tower texts, with indexes  corresponding to the id of the tower for the button.
        #endregion Game

        #region GUI
        private guiButton[] guiButtons;

        private Color mouseTowerCursorColor = new Color((int)255, (int)255, (int)255, 20); // Color of the tower cursor.

        private Int16 colR = 128; // Used for various pulsing effects.
        private Int16 colRU = 1;
        private Int16 currentSelectedTowerID = 1;
        private Texture2D currentSelectedTowerTextureBase;
        private Texture2D currentSelectedTowerTextureTurr;
        #endregion GUI

        #region Textures
        private Texture2D texBackground;
        private Texture2D texHill;
        private Texture2D texBasket;
        private Texture2D texGrid1;
        private Texture2D hudOverlay;
        #endregion Textures

        #region Fonts
        public static SpriteFont fntStageTitle;
        private SpriteFont fntGameMenu1;
        private SpriteFont fntDeathScreen;
        #endregion Fonts

        #region Input
        public static int mouseX = 0;
        public static int mouseY = 0;
        private KeyboardState thaOldKeyboard;
        private MouseState thaOldMouse;
        #endregion Input

        #region Game Objects
        public static List<Enemy> enemyRoster; // List containing all of the enemies.
        public static List<Tower> towerRoster; // List containing all of the towers.
        public static List<Projectile> projectileRoster; // List containing all of the projectiles.
        //public static int numRemoved = 0; // Number of enemies removed...broken.

        public static DChanger[] DChangers = new DChanger[24]; // Array of DChangers.
        public Rectangle[] ValidTowerSpace; //Array of valid tower locations.
        #endregion Game Objects

        public override void Init()
        {
            Logger.log("State: " + this.ToString() + " was initialized.", 2);

            gameLife = 100;
            funds = 300;
            enemyRoster = new List<Enemy>();
            towerRoster = new List<Tower>();
            projectileRoster = new List<Projectile>();

            #region DChangers
            DChangers[0] = new DChanger(0, 120, 22, 16, 35);
            DChangers[1] = new DChanger(3, 99, 124, 35, 16);
            DChangers[2] = new DChanger(1, 265, 101, 16, 35);
            DChangers[3] = new DChanger(3, 249, 15, 35, 16);
            DChangers[4] = new DChanger(0, 500, 36, 16, 35);
            DChangers[5] = new DChanger(3, 486, 145, 35, 16);
            DChangers[6] = new DChanger(0, 560, 114, 16, 41);
            DChangers[7] = new DChanger(2, 543, 282, 47, 16);
            DChangers[8] = new DChanger(1, 390, 244, 16, 28);
            DChangers[9] = new DChanger(2, 390, 165, 35, 16);
            DChangers[10] = new DChanger(0, 235, 176, 16, 30);
            DChangers[11] = new DChanger(2, 237, 288, 39, 16);
            DChangers[12] = new DChanger(1, 155, 241, 16, 33);
            DChangers[13] = new DChanger(2, 167, 165, 33, 16);
            DChangers[14] = new DChanger(0, 10, 181, 16, 35);
            DChangers[15] = new DChanger(3, 12, 359, 48, 16);
            DChangers[16] = new DChanger(0, 480, 331, 16, 33);
            DChangers[17] = new DChanger(3, 452, 423, 55, 16);
            DChangers[18] = new DChanger(0, 557, 394, 15, 35);
            DChangers[19] = new DChanger(2, 543, 496, 35, 16);
            DChangers[20] = new DChanger(1, 256, 449, 16, 38);
            DChangers[21] = new DChanger(2, 250, 392, 45, 16);
            DChangers[22] = new DChanger(0, 3, 406, 16, 34);
            DChangers[23] = new DChanger(3, 15, 590, 40, 16);
            #endregion

            #region ValidTowerSpace
            ValidTowerSpace = new Rectangle[] 
            {
            //Verticals
            //new Rectangle(0,54,100,16),
            //new Rectangle(128, 81, 107, 16),
            new Rectangle(247, 16, 266, 16),
            new Rectangle(382, 62, 98, 16),
            new Rectangle(274, 52, 98, 16),
            new Rectangle(205, 127, 55, 16),
            new Rectangle(512, 90, 70, 16),
            new Rectangle(474, 144, 69, 16),
            new Rectangle(459, 232, 91, 16),
            new Rectangle(401, 282, 173, 16),
            new Rectangle(244, 157, 176, 16),
            new Rectangle(280, 207, 125, 16),
            new Rectangle(161, 286, 115, 16),
            new Rectangle(212, 211, 29, 16),
            new Rectangle(151, 161, 52, 16),
            new Rectangle(92, 216, 84, 16),
            new Rectangle(54, 307, 90, 16),
            new Rectangle(144, 312, 354, 16),
            new Rectangle(11, 360, 257, 16),
            new Rectangle(268, 353, 186, 16),
            new Rectangle(498, 378, 70, 16),
            new Rectangle(455, 420, 90, 16),
            new Rectangle(348, 452, 199, 16),
            new Rectangle(296, 439, 52, 16),
            new Rectangle(318, 495, 253, 16),
            new Rectangle(257, 489, 61, 16),
            new Rectangle(9, 389, 279, 16), 
            new Rectangle(57, 439, 203, 16),
            new Rectangle(48, 537, 552, 16),
            new Rectangle(0, 583, 592, 16),

            //Horizontals
            //new Rectangle(128, 12, 16, 69),
            //new Rectangle(85, 70, 16, 73),
            new Rectangle(225, 16, 16, 78),
            new Rectangle(234, 16, 16, 16),
            new Rectangle(513, 16, 16, 74),
            new Rectangle(464, 78, 16, 82),
            new Rectangle(527, 160, 16, 72),
            new Rectangle(574, 90, 16, 208),
            new Rectangle(426, 157, 16, 82),
            new Rectangle(385, 223, 16, 75),
            new Rectangle(276, 222, 16, 80),
            new Rectangle(160, 232, 16, 54),
            new Rectangle(56, 245, 16, 65),
            new Rectangle(0, 262, 16, 215),
            new Rectangle(498, 312, 16, 66),
            new Rectangle(439, 369, 16, 67),
            new Rectangle(288, 389, 16, 56),
            new Rectangle(241, 455, 16, 50),
            new Rectangle(57, 455, 16, 82),
            new Rectangle(0, 396, 16, 182),
            new Rectangle(569, 378, 16, 133), 
            new Rectangle(274, 68, 16, 76), 

            new Rectangle(368, 61, 16, 15), 
            new Rectangle(442, 232, 17, 16), 
            new Rectangle(420, 157, 6, 16), 
            new Rectangle(203, 161, 16, 50), 
            new Rectangle(234, 173, 25, 17), 
            new Rectangle(235, 190, 14, 21), 
            new Rectangle(16, 405, 18, 16), 
            new Rectangle(258, 127, 16, 16), 
            new Rectangle(0, 578, 16, 6), 
            };
            #endregion

            #region Info text
            infoTexts = new String[]
            {
                "Null Tower" + "\n" + 
                "--------------------" + "\n" + 
                "Cost "+ Tower.costs[0] + "\n" + 
                "This is a test tower to test " + "\n" + 
                "the tower system. It has a " + "\n" + 
                "purple streak." ,
                
                "Basic Ant Shooter" + "\n" + 
                "--------------------" + "\n" + 
                "Cost "+ Tower.costs[1] + "\n" + 
                "A basic ant shooter with a " + "\n" + 
                "standard range. This tower will " + "\n" + 
                "get basic jobs done." ,
                
                "Water Tower" + "\n" + 
                "--------------------" + "\n" + 
                "Cost "+ Tower.costs[2] + "\n" + 
                "A fast shooting tower that " + "\n" + 
                "slows ants down."
            };
            #endregion

            #region GUI Buttons
            guiButtons = new guiButton[]
            {
                new guiButton(2, "Start", new Vector2(734,560),Keys.Space),
                //new guiButton(3, null, new Vector2(620,104),Keys.D1, 0),
                new guiButton(3, null, new Vector2(620,104),Keys.D1, 1),
                new guiButton(3, null, new Vector2(660,104),Keys.D2, 2)
            };
            #endregion GUI Buttons

            #region Bordem
            Boolean useBordem = false;
            if (useBordem)
            {
                for (int i = 0; i < 900; i += 16)
                {
                    for (int j = 0; j < 900; j += 16)
                    {
                        
                        for (int k = 0; k < ValidTowerSpace.Length; k++)
                        {
                            if (ValidTowerSpace[k].Contains(new Point(i, j)))
                            {
                                towerRoster.Add(new Tower(2, i, j));
                            }
                        }
                    }
                }
            }
            #endregion Bordem
            
        }

        public override void Update()
        {
            if (Game1.tickCount % 50 == 0)
            {
                Logger.log("Enemies: " + numOfEnemies);
            }
            // Keep life in bounds.
            if (gameLife < 0)
            {
                gameLife = 0;
            }

            // Update guiButtons.
            for (int i = 0; i < guiButtons.Length; i++)
            {
                guiButtons[i].Update();
            }

            // Increment spawn timer.
            time = time += 1;
            if (time >= (Int64.MaxValue - 10))
            {
                time = 0;
            }

            // Update enemy roster.
            for (int i = 0; i < enemyRoster.Count; i++)
            {
                enemyRoster[i].Update();
            }
            UpdateEnemies();

            // Update tower roster.
            for (int i = 0; i < towerRoster.Count; i++)
            {
                towerRoster[i].Update();
            }

            // Update projectile roster.
            for (int i = 0; i < projectileRoster.Count; i++)
            {
                projectileRoster[i].Update();
            }

            // Update based on SubState.
            switch (SubState)
            {
                case 0:
                    guiButtons[0].active = true;
                    break;
                case 1:
                    guiButtons[0].active = false;
                    //if (finalAntSpawned)
                    //{
                    //    if (finalAnt.alive == false)
                    //    {
                    //        currentRound++;
                    //        finalAntSpawned = false;
                    //        SubState = 0;
                    //        numRemoved = 0;
                    //    }
                    //}
                    if (numOfEnemies <= 0)
                    {
                        currentRound++;
                        SubState = 0;
                    }
                    if (gameLife <= 0)
                    {
                        RemoveAllEnemies();
                        SubState = 2;
                    }
                    break;
            }

            // Increment R value for title color.
            if (colRU == 1)
            {
                colR = colR += 1;
            }
            else
            {
                colR = colR -= 1;
            }
            if (colR >= 255)
            {
                colRU = 0;
            }
            if (colR <= 127)
            {
                colRU = 1;
            }

            // Update mouse cursor color.
            switch (isMouseInValidArea)
            {
                case true:
                    mouseTowerCursorColor = new Color((int)0, (int)255, (int)0, 128);
                    break;
                case false:
                    mouseTowerCursorColor = new Color((int)255, (int)0, (int)0, 128);
                    break;
            }

            // Set currently selected textures correctly.
            switch (currentSelectedTowerID)
            {
                case 0:
                    currentSelectedTowerTextureBase = Game1.texTestTower;
                    currentSelectedTowerTextureTurr = Game1.texTestTowerTurret;
                    break;
                case 1:
                    currentSelectedTowerTextureBase = Game1.texBasicAntShooter;
                    currentSelectedTowerTextureTurr = Game1.texBasicAntShooterTurret;
                    break;
                case 2:
                    currentSelectedTowerTextureBase = Game1.texWaterTower;
                    currentSelectedTowerTextureTurr = Game1.texWaterTowerTurret;
                    break;
            }
        }

        public override void LoadContent()
        {
            #region Fonts
            fntStageTitle = Program.game.LoadFont("StageTitle");
            fntGameMenu1 = Program.game.LoadFont("fntGameMenu1");
            fntDeathScreen = Program.game.LoadFont("fntDeathScreen");
            #endregion Fonts

            #region Textures
            texBackground = Program.game.LoadTexture("Stage1Background");
            texHill = Program.game.LoadTexture("Stage1BackgroundHill");
            texBasket = Program.game.LoadTexture("basket");
            texGrid1 = Program.game.LoadTexture("debug/8_8gridOverlay");
            hudOverlay = Program.game.LoadTexture("gui/hudOverlay");
            currentSelectedTowerTextureBase = Game1.texError;
            currentSelectedTowerTextureTurr = Game1.texError;
            #endregion Textures

            // Cycle guiButtons.
            for (int i = 0; i < guiButtons.Length; i++)
            {
                guiButtons[i].LoadContent();
            }
        }

        public override void UnloadContent()
        {

        }

        public override void Input(KeyboardState thaKeyboard, MouseState thaMouse)
        {
            mouseX = thaMouse.X;
            mouseY = thaMouse.Y;

            //DEBUG create enemy if LeftMouse and LeftControl are pressed.
            //if (InputManager.IsMouseLeftPressed(thaMouse, thaOldMouse) && (thaKeyboard.IsKeyDown(Keys.D)))
            //{
            //    CreateEnemy(2, mouseX, mouseY, 3);
            //}

            // Check input based on SubState.                
            switch (SubState)
            {
                case 0:
                    // Check if mouse is in valid position for placing towers.
                    for (int i = 0; i < ValidTowerSpace.Length; i++)
                    {
                        if (ValidTowerSpace[i].Contains(new Point(mouseX, mouseY)))
                        {
                            if (towerRoster.Count > 0)
                            {
                                for (int j = 0; j < towerRoster.Count; j++)
                                {
                                    if (Vector2.Distance(new Vector2(mouseX, mouseY), new Vector2(towerRoster[j].pos.X + (towerRoster[j].radius / 8), towerRoster[j].pos.Y + (towerRoster[j].radius / 8))) <= 32)
                                    {
                                        isMouseInValidArea = false;
                                    }
                                    else
                                    {
                                        isMouseInValidArea = true;
                                    }
                                }
                            }
                            else
                            {
                                isMouseInValidArea = true;
                            }
                            break;
                        }
                        else
                        {
                            isMouseInValidArea = false;
                        }
                    }
                    // If so add a tower when clicked.
                    if (InputManager.IsMouseLeftPressed(thaMouse, thaOldMouse) && isMouseInValidArea)
                    {
                        if (funds >= Tower.costs[currentSelectedTowerID])
                        {
                            funds -= Tower.costs[currentSelectedTowerID];
                            towerRoster.Add(new Tower(currentSelectedTowerID, mouseX, mouseY));
                            Game1.audTowerPlace.Play(Game1.SFX_VOLUME, 0.0f, 0.0f);
                        }
                    }

                    // Check start round button input.
                    if (guiButtons[0].isButtonClicked())
                    {
                        SubState = 1;
                        SpawnEnemies(currentRound);
                        guiButtons[0].Reset();
                    }
                    break;
                case 2:
                    // If dead, go to the main menu when space is pressed.
                    if (InputManager.IsKeyPressed(Keys.Space, thaKeyboard, thaOldKeyboard))
                    {
                        Program.game.sm.SetState(new StateMenu());
                    }
                    break;
            }

            //DEBUG Spawn enemy round 1.
            //if (InputManager.IsKeyPressed(Keys.C, thaKeyboard, thaOldKeyboard))
            //{
            //    SpawnEnemies(1);
            //}

            //DEBUG Health cheat
            if ((thaKeyboard.IsKeyDown(Keys.D)) && (InputManager.IsKeyPressed(Keys.C, thaKeyboard, thaOldKeyboard)))
            {
                if (StateMenu.netSplash == "Cheats!")
                {
                    Cheats cheats = new Cheats();
                    cheats.Show();
                }
            }

            // Cycle guiButtons.
            for (int i = 0; i < guiButtons.Length; i++)
            {
                guiButtons[i].Input(thaMouse, thaKeyboard, thaOldKeyboard);
                if ((SubState == 0) && (guiButtons[i].buttonState == 1 || guiButtons[i].buttonState == 2))
                {
                    infoText = infoTexts[guiButtons[i].tower];
                    break;
                }
                else
                {
                    infoText = "";
                }
            }

            // Update info text and selected tower based on guiButtons.
            for (int i = 0; i < guiButtons.Length; i++)
            {
                if (guiButtons[i].type == 3)
                {
                    if (funds < Tower.costs[guiButtons[i].tower])
                    {
                        guiButtons[i].active = false;
                    }
                    else
                    {
                        guiButtons[i].active = true;
                    }
                    if (guiButtons[i].isButtonClicked())
                    {
                        currentSelectedTowerID = guiButtons[i].tower;
                        break;
                    }
                }
            }

            thaOldMouse = thaMouse;
            thaOldKeyboard = thaKeyboard;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            #region Background
            //Draw background
            spriteBatch.Draw(texBackground, new Rectangle(0, 0, 600, 600), Color.White);
            #endregion Background

            #region Game Objects
            //Draw enemy roster
            for (int i = 0; i < enemyRoster.Count; i++)
            {
                enemyRoster[i].Draw(spriteBatch);
            }

            //Draw tower roster
            for (int i = 0; i < towerRoster.Count; i++)
            {
                towerRoster[i].Draw(spriteBatch);
            }

            //Draw projectile roster
            for (int i = 0; i < projectileRoster.Count; i++)
            {
                projectileRoster[i].Draw(spriteBatch);
            }

            spriteBatch.Draw(texHill, new Rectangle(0,0,600,600), new Color(255,255,255,128));
            #endregion Game Objects

            #region Overlays


            if (CVar.overlays == 1)
            {
                spriteBatch.Draw(Game1.texWhite, spawnPoint, new Color(0, 255, 0, 20));

                //Draw DChangers
                for (int i = 0; i < DChangers.Length; i++)
                {
                    DChangers[i].Draw(spriteBatch);
                }

                //Draw valid tower locations
                for (int i = 0; i < ValidTowerSpace.Length; i++)
                {
                    spriteBatch.Draw(Game1.texWhite, ValidTowerSpace[i], new Color(255, 0, 255, (int)100));
                    //spriteBatch.Draw(Game1.texWhite, ValidTowerSpace[i], new Color(255, 0, 255, (int)255));
                }
            }

            if (CVar.grid == 1)
            {
                //Draw grid
                spriteBatch.Draw(texGrid1, new Rectangle(0, 0, 854, 600), Color.Red);
            }
            #endregion Overlays

            #region GUI
            //Draw selection cursor
            if (SubState == 0)
            {
                spriteBatch.Draw(currentSelectedTowerTextureBase, new Rectangle(mouseX, mouseY, 32, 32), null, mouseTowerCursorColor, 0, new Vector2(32 / 2, 32 / 2), SpriteEffects.None, 0);
                spriteBatch.Draw(currentSelectedTowerTextureTurr, new Rectangle(mouseX, mouseY, 32, 32), null, mouseTowerCursorColor, 0, new Vector2(32 / 2, 32 / 2), SpriteEffects.None, 0);
            }

            //Draw hud base
            spriteBatch.Draw(hudOverlay, new Rectangle(0, 0, Game1.GAME_WIDTH, Game1.GAME_HEIGHT), Color.White);

            //Cycle-n-draw the hud buttons
            for (int i = 0; i < guiButtons.Length; i++)
            {
                guiButtons[i].Draw(spriteBatch);
            }

            //Draw info text
            spriteBatch.DrawString(fntStageTitle, infoText, new Vector2(620, 230), Color.Black);

            //Draw various text
            spriteBatch.DrawString(fntGameMenu1, "" + gameLife, new Vector2(654, 556), Color.White);
            spriteBatch.DrawString(fntGameMenu1, "" + funds, new Vector2(654, 519), new Color(105, 255, 110));
            spriteBatch.DrawString(fntStageTitle, "Round: " + currentRound, new Vector2(267, 0), Color.White);
            spriteBatch.DrawString(fntStageTitle, Game1.GAME_TITLE + " by " + Game1.GAME_AUTHOR, new Vector2(0, 0), new Color(colR, 0, 0));
            
            // Draw Basket.
            spriteBatch.Draw(texBasket, rectBasket, Color.White);
            #endregion GUI

            #region Death Screen
            if (SubState == 2)
            {
                spriteBatch.Draw(Game1.texWhite, new Rectangle(0, 0, 854, 600), new Color(255, 0, 0, 128));
                spriteBatch.DrawString(fntDeathScreen, "You have died!", new Vector2(84, 98), new Color(0, colR - 30, colR)); spriteBatch.DrawString(fntDeathScreen, "You have died!", new Vector2(84, 98), new Color(0, colR - 30, colR));
                spriteBatch.DrawString(fntGameMenu1, "Press space to go to the main menu.", new Vector2(84, 193), new Color(0, colR, colR - 30));
            }
            #endregion Death Screen
        }

        public override void DeInit()
        {
        }

        /// <summary>
        /// Updates enemy roster based on round and time
        /// </summary>
        public void UpdateEnemies()
        {
            switch (spawnRound)
            {
                #region Round1
                case 1:
                    switch (time)
                    {
                        case 1: CreateEnemy(1, 3); break;
                        case 23: CreateEnemy(1, 3); break;
                        case 46: CreateEnemy(1, 3); break;
                        case 63: CreateEnemy(1, 3); break;
                        case 79: CreateEnemy(1, 3); break;
                        case 96: CreateEnemy(1, 3); break;
                        case 110: CreateEnemy(1, 3); break;
                        case 170: CreateEnemy(1, 3); break;
                        case 193: CreateEnemy(1, 3); break;
                        case 210:
                            CreateEnemy(1, 3);
                            //finalAnt = enemyRoster[enemyRoster.Count - 1];
                            //finalAntSpawned = true;
                            spawnRound = 0;
                            break;
                    }
                    break;
                #endregion

                #region Round2
                case 2:

                    switch (time)
                    {
                        case 1: CreateEnemy(1, 3); break;
                        case 23: CreateEnemy(1, 3); break;
                        case 46: CreateEnemy(1, 3); break;
                        case 80: CreateEnemy(2, 3); break;
                        case 113: CreateEnemy(2, 3); break;
                        case 135:
                            CreateEnemy(1, 3);
                            //finalAnt = enemyRoster[enemyRoster.Count - 1];
                            //finalAntSpawned = true;
                            spawnRound = 0;
                            break;
                    }

                    break;
                #endregion

                #region Round3
                case 3:
                    switch (time)
                    {
                        case 1: CreateEnemy(1, 3); break;
                        case 20: CreateEnemy(1, 3); break;
                        case 40: CreateEnemy(1, 3); break;
                        case 60: CreateEnemy(1, 3); break;
                        case 85: CreateEnemy(1, 3); break;
                        case 100: CreateEnemy(2, 3); break;
                        case 120: CreateEnemy(2, 3); break;
                        case 140: CreateEnemy(3, 3); break;
                        case 160: CreateEnemy(3, 3); break;
                        case 180: CreateEnemy(2, 3); break;
                        case 190: 
                            CreateEnemy(2, 3); 
                            spawnRound = 0;
                            break;
                    }
                    break;
                #endregion
            }
        }

        /// <summary>
        /// Create an enemy of the given type facing the given direction
        /// </summary>
        /// <param name="type">0 = none, 1 = Black Ant, 2 = Army Ant;</param>
        /// <param name="dir">0 = Down, 1 = Up, 2 = Left, 3 = Right;</param>
        public void CreateEnemy(int type, int dir)
        {
            Int32 id = enemyRoster.Count;
            enemyRoster.Add(new Enemy(type, spawnPoint.X - 5, rGen.Next((spawnPoint.Y + spawnPoint.Height) - 32, spawnPoint.Y), dir, id));
        }

        /// <summary>
        /// Create an enemy of the given type at the given location facing the given direction
        /// </summary>
        /// <param name="type">0 = none, 1 = Black Ant, 2 = Army Ant;</param>
        /// <param name="x">X coord of enemy.</param>
        /// <param name="y">Y coord of enemy.</param>
        /// <param name="dir">0 = Down, 1 = Up, 2 = Left, 3 = Right;</param>
        public void CreateEnemy(int type, int x, int y, int dir)
        {
            int id = enemyRoster.Count - 1;
            enemyRoster.Add(new Enemy(type, x, y, dir, id));
        }

        /// <summary>
        /// Spawns the enemies for the given round
        /// </summary>
        /// <param name="round"></param>
        public void SpawnEnemies(int round)
        {
            spawnRound = round;
            time = 0;
        }

        /// <summary>
        /// Removes all enemies in the map
        /// </summary>
        public void RemoveAllEnemies()
        {
            for (int i = 0; i < enemyRoster.Count; i++)
            {
                enemyRoster[i].Remove(false);
            }
        }
    }
}
