﻿//--------------------------------------------------------------------------------//
// Basket Defense | Editor.cs-----------------------------------------------------//
// By: Phillip Hunter-------------------------------------------------------------//
// Desc: Developer tool menu.-----------------------------------------------------//
//--------------------------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BasketDefenseEditor
{
    public partial class Editor : Form
    {
        Form scriptEditor = new ScriptEditor();
        public Editor()
        {
            InitializeComponent();
        }

        private void btnScript_Click(object sender, EventArgs e)
        {
            if (scriptEditor.IsDisposed)
            {
                scriptEditor = new ScriptEditor();
            }
            scriptEditor.Show();
        }
    }
}
