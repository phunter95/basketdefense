﻿namespace BasketDefenseEditor
{
    partial class ScriptEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtScript = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtLine = new System.Windows.Forms.TextBox();
            this.txtRound = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radFireAnt = new System.Windows.Forms.RadioButton();
            this.radArmyAnt = new System.Windows.Forms.RadioButton();
            this.radBlackAnt = new System.Windows.Forms.RadioButton();
            this.grpOptions = new System.Windows.Forms.GroupBox();
            this.txtDirection = new System.Windows.Forms.TextBox();
            this.lblDirection = new System.Windows.Forms.Label();
            this.txtSpeed = new System.Windows.Forms.TextBox();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.txtStep = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.grpOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtScript
            // 
            this.txtScript.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtScript.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtScript.Location = new System.Drawing.Point(495, 0);
            this.txtScript.Multiline = true;
            this.txtScript.Name = "txtScript";
            this.txtScript.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtScript.Size = new System.Drawing.Size(348, 457);
            this.txtScript.TabIndex = 0;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(277, 237);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // txtLine
            // 
            this.txtLine.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLine.Location = new System.Drawing.Point(12, 319);
            this.txtLine.Name = "txtLine";
            this.txtLine.Size = new System.Drawing.Size(340, 20);
            this.txtLine.TabIndex = 2;
            // 
            // txtRound
            // 
            this.txtRound.Location = new System.Drawing.Point(60, 12);
            this.txtRound.Name = "txtRound";
            this.txtRound.Size = new System.Drawing.Size(26, 20);
            this.txtRound.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Round:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radFireAnt);
            this.groupBox1.Controls.Add(this.radArmyAnt);
            this.groupBox1.Controls.Add(this.radBlackAnt);
            this.groupBox1.Location = new System.Drawing.Point(21, 62);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(331, 46);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enemy Type";
            // 
            // radFireAnt
            // 
            this.radFireAnt.AutoSize = true;
            this.radFireAnt.Location = new System.Drawing.Point(156, 19);
            this.radFireAnt.Name = "radFireAnt";
            this.radFireAnt.Size = new System.Drawing.Size(61, 17);
            this.radFireAnt.TabIndex = 2;
            this.radFireAnt.Text = "Fire Ant";
            this.radFireAnt.UseVisualStyleBackColor = true;
            // 
            // radArmyAnt
            // 
            this.radArmyAnt.AutoSize = true;
            this.radArmyAnt.Location = new System.Drawing.Point(83, 19);
            this.radArmyAnt.Name = "radArmyAnt";
            this.radArmyAnt.Size = new System.Drawing.Size(67, 17);
            this.radArmyAnt.TabIndex = 1;
            this.radArmyAnt.Text = "Army Ant";
            this.radArmyAnt.UseVisualStyleBackColor = true;
            // 
            // radBlackAnt
            // 
            this.radBlackAnt.AutoSize = true;
            this.radBlackAnt.Checked = true;
            this.radBlackAnt.Location = new System.Drawing.Point(6, 19);
            this.radBlackAnt.Name = "radBlackAnt";
            this.radBlackAnt.Size = new System.Drawing.Size(71, 17);
            this.radBlackAnt.TabIndex = 0;
            this.radBlackAnt.TabStop = true;
            this.radBlackAnt.Text = "Black Ant";
            this.radBlackAnt.UseVisualStyleBackColor = true;
            // 
            // grpOptions
            // 
            this.grpOptions.Controls.Add(this.txtDirection);
            this.grpOptions.Controls.Add(this.lblDirection);
            this.grpOptions.Controls.Add(this.txtSpeed);
            this.grpOptions.Controls.Add(this.lblSpeed);
            this.grpOptions.Controls.Add(this.txtStep);
            this.grpOptions.Controls.Add(this.label2);
            this.grpOptions.Location = new System.Drawing.Point(21, 114);
            this.grpOptions.Name = "grpOptions";
            this.grpOptions.Size = new System.Drawing.Size(331, 100);
            this.grpOptions.TabIndex = 6;
            this.grpOptions.TabStop = false;
            this.grpOptions.Text = "Script Options";
            // 
            // txtDirection
            // 
            this.txtDirection.Location = new System.Drawing.Point(72, 76);
            this.txtDirection.Name = "txtDirection";
            this.txtDirection.Size = new System.Drawing.Size(30, 20);
            this.txtDirection.TabIndex = 5;
            this.txtDirection.Text = "1";
            // 
            // lblDirection
            // 
            this.lblDirection.AutoSize = true;
            this.lblDirection.Location = new System.Drawing.Point(15, 79);
            this.lblDirection.Name = "lblDirection";
            this.lblDirection.Size = new System.Drawing.Size(55, 13);
            this.lblDirection.TabIndex = 4;
            this.lblDirection.Text = "Direction: ";
            // 
            // txtSpeed
            // 
            this.txtSpeed.Enabled = false;
            this.txtSpeed.Location = new System.Drawing.Point(72, 48);
            this.txtSpeed.Name = "txtSpeed";
            this.txtSpeed.Size = new System.Drawing.Size(30, 20);
            this.txtSpeed.TabIndex = 3;
            // 
            // lblSpeed
            // 
            this.lblSpeed.AutoSize = true;
            this.lblSpeed.Location = new System.Drawing.Point(15, 51);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(44, 13);
            this.lblSpeed.TabIndex = 2;
            this.lblSpeed.Text = "Speed: ";
            // 
            // txtStep
            // 
            this.txtStep.Location = new System.Drawing.Point(72, 22);
            this.txtStep.Name = "txtStep";
            this.txtStep.Size = new System.Drawing.Size(30, 20);
            this.txtStep.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Step: ";
            // 
            // ScriptEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 457);
            this.Controls.Add(this.grpOptions);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtRound);
            this.Controls.Add(this.txtLine);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtScript);
            this.Name = "ScriptEditor";
            this.Text = "ScriptEditor";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpOptions.ResumeLayout(false);
            this.grpOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtScript;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox txtLine;
        private System.Windows.Forms.TextBox txtRound;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radArmyAnt;
        private System.Windows.Forms.RadioButton radBlackAnt;
        private System.Windows.Forms.GroupBox grpOptions;
        private System.Windows.Forms.TextBox txtStep;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSpeed;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.TextBox txtDirection;
        private System.Windows.Forms.Label lblDirection;
        private System.Windows.Forms.RadioButton radFireAnt;
    }
}