﻿//---------------------------------------------------------------//
// Basket Defense | ScriptEditor.cs------------------------------//
// By: Phillip Hunter--------------------------------------------//
// Desc: GUI for scripting stage sequences.----------------------//
//---------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BasketDefenseEditor
{
    public partial class ScriptEditor : Form
    {
        String __ = System.Environment.NewLine;
        String Script = "";
        String Line = "";
        String LineEnemyType = "";
        String LineStep = "";
        String LineSpeed = "";
        String LineDir = "";
        String Round = "";

        public ScriptEditor()
        {
            InitializeComponent();
        }

        List<String> body = new List<String>();

        private void timer1_Tick(object sender, EventArgs e)
        {
            Script =
                "#region Round" + Round + __ +
                "case " + Round + ":" + __ +
                "   switch(time)" + __ +
                "   {" + __;

            for (int i = 0; i < body.Count; i++)
            {
                Script += body[i];
            }

            Script +=
                "   }" + __ +
                "   break;" + __ +
                "#endregion"
            ;

            if (radBlackAnt.Checked == true)
            {
                LineEnemyType = "1";
            }
            if (radArmyAnt.Checked == true)
            {
                LineEnemyType = "2";
            }
            if (radFireAnt.Checked == true)
            {
                LineEnemyType = "3";
            }

            Round = txtRound.Text;
            LineStep = txtStep.Text;
            LineSpeed = txtSpeed.Text;
            LineDir = txtDirection.Text;

            //Line = "case " + LineStep + ": CreateEnemy(" + LineEnemyType + ", " + LineSpeed + "f," + LineDir + "); break;";
            Line = "case " + LineStep + ": CreateEnemy(" + LineEnemyType + ", " + LineDir + "); break;";

            if ((txtRound.Text == "") || (txtStep.Text == "") || (txtDirection.Text == ""))
            {
                txtLine.ForeColor = Color.Red;
            }
            else
            {
                txtLine.ForeColor = Color.Black;
            }

            txtLine.Text = Line;
            txtScript.Text = Script;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            body.Add("      " + Line + __);
        }
    }
}
