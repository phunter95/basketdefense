﻿//---------------------------------------------------------------//
// Basket Defense | Logger.cs------------------------------------//
// By: Phillip Hunter--------------------------------------------//
// Desc: Logs information in an organized way to the console.----//
//---------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace BasketDefense
{
    class Logger
    {
        private static String logFilePath = "log.txt"; // Where to save the log file.
        private static StringBuilder sb = new StringBuilder();

        /// <summary>
        /// Logs the text in an organized fashion to the console.
        /// </summary>
        /// <param name="str">Text to log.</param>
        /// <param name="level">0: Plain text 1: Info 2: System 3: Error</param>
        public static void log(String str, Int32 level)
        {
            DateTime time = DateTime.Now;
            String prefix = "<" + time.ToShortDateString() + " " + time.ToLongTimeString() + ">";
            switch (level)
            {
                case 0:
                    Console.WriteLine(" ");
                    Console.WriteLine(prefix);
                    Console.WriteLine(str);
                    sb.AppendLine();
                    sb.AppendLine(prefix + " " + str);
                    break;
                case 1:
                    Console.WriteLine(" ");
                    Console.WriteLine(prefix);
                    Console.WriteLine("[Info] " + str);
                    sb.AppendLine();
                    sb.AppendLine(prefix + "[Info] " + str);
                    break;
                case 2:
                    Console.WriteLine(" ");
                    Console.WriteLine(prefix);
                    Console.WriteLine("[System] " + str);
                    sb.AppendLine();
                    sb.AppendLine(prefix + "[System] " + str);
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" ");
                    Console.WriteLine(prefix);
                    Console.Error.WriteLine("[Error] " + str);
                    sb.AppendLine();
                    sb.AppendLine(prefix + "[Error] " + str);
                    Console.ForegroundColor = Game1.consoleColor;
                    break;
            }

            using (StreamWriter outfile = new StreamWriter(logFilePath))
            {
                outfile.Write(sb.ToString());
            }
        }

        /// <summary>
        /// Simply logs the text to the console.
        /// </summary>
        /// <param name="str">Text to log.</param>
        public static void log(String str)
        {
            Console.WriteLine(str);
            sb.AppendLine(str);
            //using (StreamWriter outfile = new StreamWriter(logFilePath))
            //{
            //    outfile.WriteLine(sb.ToString());
            //}
        }

        public static void test()
        {
            if (Game1.tickCount % 50 == 0)
            {
                Console.WriteLine("===TEST===");
            }
        }
    }
}
