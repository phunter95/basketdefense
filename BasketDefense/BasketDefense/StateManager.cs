﻿//---------------------------------------------------------------//
// Basket Defense | StateManager.cs------------------------------//
// By: Phillip Hunter--------------------------------------------//
// Desc: Manages the active state.-------------------------------//
//---------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BasketDefense
{
    public class StateManager
    {   
        public State state = new StateIntroScreen(); //The active state.

        public void Init()
        {
            state.Init();
        }

        public void LoadContent()
        {
            state.LoadContent();
        }

        public void UnloadContent()
        {

        }
        
        public void Update()
        {
            state.Update();
        }
        
        public void Input(KeyboardState thaKeyboard, MouseState thaMouse)
        {
            state.Input(thaKeyboard, thaMouse);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            state.Draw(spriteBatch);
        }

        public void DeInit()
        {
            state.DeInit();
        }

        public void SetState(State newState)
        {
            state.DeInit();
            state = newState;
            state.Init();
            state.LoadContent();
        }
    }
}
