﻿//--------------------------------------------------------------------------------//
// Basket Defense | Enemy.cs------------------------------------------------------//
// By: Phillip Hunter-------------------------------------------------------------//
// Desc: Game entities that will move to the target and be attacked by towers.----//
//--------------------------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BasketDefense
{
    class Enemy
    {
        public Int32 type; //0 = none, 1 = Black Ant, 2 = Army Ant;
        public Vector2 pos;
        private Int32 direction; // 0 = Down, 1 = Up, 2 = Left, 3 = Right;
        //TODO Fix this ID system.
        //public Int32 ID;

        public Single health;
        private Single maxHp;
        private Int32 power; // Damage to gameLife.
        private Rectangle hpBar;
        public Rectangle bounds;
        private Single speed;
        public Boolean alive = true;
        private Color color;

        private Texture2D texture;
        private Texture2D textureL;
        private Texture2D textureR;
        private Texture2D textureD;
        private Texture2D textureU;

        #region Status
        //ATINDEX: 0 = Watered
        public Int16[] status = new Int16[] {0};
        public Int64[] statusTimer = new Int64[] {-1};
        #endregion Status

        //DEBUG
        private MouseState thaMouse;
        private MouseState thaOldMouse;

        /// <summary>
        /// Creates a game enemy object.
        /// </summary>
        /// <param name="type">0 = Null Ant, 1 = Black Ant, 2 = Army Ant;</param>
        /// <param name="x">X coordinate to spawn enemy.</param>
        /// <param name="y">Y coordinate to spawn enemy.</param>
        /// <param name="direction">0 = Down, 1 = Up, 2 = Left, 3 = Right;</param>
        /// <param name="id"></param>
        public Enemy(Int32 type, Int32 x, Int32 y, Int32 direction, Int32 id)
        {
            this.type = type;
            this.pos.X = x;
            this.pos.Y = y;
            this.direction = direction;
            //this.hpBar = new Rectangle(x, y - 3, health * 3, 8);
            //this.ID = id;
            StateMap1.numOfEnemies++;
            switch (this.type)
            {
                case 0: // Null ant
                    texture = Game1.texError;
                    textureL = Game1.texError;
                    textureR = Game1.texError;
                    textureD = Game1.texError;
                    textureU = Game1.texError;
                    speed = 1.0f;
                    health = 1;
                    power = 0;
                    color = new Color(255, 255, 255);
                    break;
                case 1: // Black Ant
                    texture = Game1.texBlackAntR;
                    textureL = Game1.texBlackAntL;
                    textureR = Game1.texBlackAntR;
                    textureD = Game1.texBlackAntD;
                    textureU = Game1.texBlackAntU;
                    speed = 4.0f;
                    health = 2;
                    power = 1;
                    color = new Color(30,0,0);
                    break;
                case 2: // Army Ant
                    texture = Game1.texArmyAntR;
                    textureL = Game1.texArmyAntL;
                    textureR = Game1.texArmyAntR;
                    textureD = Game1.texArmyAntD;
                    textureU = Game1.texArmyAntU;
                    speed = 4.75f;
                    health = 3;
                    power = 2;
                    color = new Color(255, 255, 255);
                    break;
                case 3: // Fire Ant
                    texture = Game1.texFireAntR;
                    textureL = Game1.texFireAntL;
                    textureR = Game1.texFireAntR;
                    textureD = Game1.texFireAntD;
                    textureU = Game1.texFireAntU;
                    speed = 5.85f;
                    health = 5;
                    power = 3;
                    color = new Color(255, 255, 255);
                    break;
            }

            maxHp = health; // Set the max health.
        }

        public void Update()
        {
            if (alive)
            {
                // Remove if health is less than/equal to 0.
                if (this.health <= 0)
                {
                    this.Remove(true);
                }

                // Update rectangle bounds.
                bounds = new Rectangle((int)pos.X, (int)pos.Y, 32, 32);

                // Remove if in collision with target.
                if ((bounds.Intersects(StateMap1.rectBasket)))
                {
                    StateMap1.gameLife -= this.power;
                    this.Remove(false);
                }

                // Change direction on collision with DChangers.
                for (int i = 0; i < StateMap1.DChangers.Length; i++)
                {
                    if (bounds.Intersects(StateMap1.DChangers[i].coll))
                    {
                        this.direction = StateMap1.DChangers[i].dir;
                    }
                }

                // Decrease health on collision with projectile.
                for (int i = 0; i < StateMap1.projectileRoster.Count; i++)
                {
                    if (bounds.Intersects(StateMap1.projectileRoster[i].coll))
                    {
                        this.health -= StateMap1.projectileRoster[i].power;
                    }
                }

                //Apply "Watered" status
                if (this.status[0] == 1)
                {
                    this.speed = speed / 2;
                    this.statusTimer[0] -= 1;
                    if (this.type == 1)
                    {
                        color = new Color(20, 0, 50);
                    }
                    else
                    {
                        color = new Color(128, 128, 255);
                    }
                }
                // Change position and texture based on direction.
                switch (direction)
                {
                    case 0:
                        pos.Y += speed;
                        texture = textureD;
                        break;
                    case 1:
                        pos.Y -= speed;
                        texture = textureU;
                        break;
                    case 2:
                        pos.X -= speed;
                        texture = textureL;
                        break;
                    case 3:
                        pos.X += speed;
                        texture = textureR;
                        break;
                }
                //Apply "Watered" status
                if (this.status[0] == 1)
                {
                    this.speed = speed * 2;
                    if (this.statusTimer[0] == 1)
                    {
                        this.status[0] = 0;
                        if (this.type == 1)
                        {
                            color = new Color(20, 0, 0);
                        }
                        else
                        {
                            color = new Color(255, 255, 255);
                        }
                    }
                }

                //DEBUG Show info about this Enemy when clicked.
                thaMouse = Mouse.GetState();
                //if ((bounds.Contains(new Point(StateMap1.mouseX, StateMap1.mouseY))) && (InputManager.IsMouseLeftPressed(thaMouse, thaOldMouse)))
                //{
                //    Logger.log("Enemy with ID " + ID + " was clicked.");
                //}
                thaOldMouse = thaMouse;
            }

            //TODO Fix this
            // Update HP bar.
            //this.hpBar = new Rectangle((int) (this.pos.X), (int) (this.pos.Y) - 3, 20 / maxHp + health, 8);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (this.alive)
            {
                // Draw the enemy.
                spriteBatch.Draw(texture, bounds, color);

                // Draw the ID number above the enemy.
                //if (CVar.overlays == 1)
                //{
                //    spriteBatch.DrawString(StateMap1.fntStageTitle, "" + ID, new Vector2(this.pos.X, this.pos.Y - 5), Color.White);
                //}
                // Draw HP bar spriteBatch.Draw(Game1.texWhite, hpBar, Color.Red);
            }
        }

        public void Remove(Boolean giveFunds)
        {
            if (alive)
            {
                //Logger.log("Num removed: " + StateMap1.numRemoved);
                //Logger.log("Removing ant with id " + this.ID);
                //Logger.log("Removing at index position " + (this.ID - StateMap1.numRemoved));
                //try
                //{
                //    StateMap1.enemyRoster.RemoveAt(this.ID - StateMap1.numRemoved);
                //}
                //catch (Exception) //TODO Make this work ALOT better.
                //{
                //    StateMap1.enemyRoster.RemoveAt(0);
                //}
                //StateMap1.numRemoved++;

                if (giveFunds == true)
                {
                    switch (this.type)
                    {
                        case 0:
                            StateMap1.funds += 9000;
                            break;
                        case 1:
                            StateMap1.funds += 5;
                            break;
                        case 2:
                            StateMap1.funds += 10;
                            break;
                        case 3:
                            StateMap1.funds += 20;
                            break;
                    }
                }
                StateMap1.numOfEnemies--;
            }
            this.alive = false;
        }

    }
}
