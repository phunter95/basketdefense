﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BasketDefense
{
    class StateTutorial:State
    {
        public override void Init()
        {

        }
        public override void LoadContent()
        {

        }
        public override void UnloadContent()
        {

        }
        public override void Update()
        {

        }
        public override void Input(KeyboardState thaKeyboard, MouseState thaMouse)
        {

        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Game1.texError, new Microsoft.Xna.Framework.Rectangle(0,0,Game1.GAME_WIDTH, Game1.GAME_HEIGHT), Color.White);
        }
        public override void DeInit()
        {

        }
    }
}
