﻿//---------------------------------------------------------------//
// Basket Defense | StateMenu.cs---------------------------------//
// By: Phillip Hunter--------------------------------------------//
// Desc: State for the Main Menu.--------------------------------//
//---------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Net;
using BasketDefenseEditor;

namespace BasketDefense
{
    class StateMenu : State
    {
        //DEBUG
        private KeyboardState thaOldKeyboard;
        private Int32 mouseX = 0;
        private Int32 mouseY = 0;

        private guiButton[] buttons;

        public static Texture2D texTitleCard;
        private SpriteFont fntSplash;
        private SpriteFont fntVersion;

        #region Splash System
        private String[] splashes;
        private String currentSplash;
        private Random r = new Random();
        private Single splashMod = 1;
        private Int32 splashModDir = 1;
        private Color splashCol = Color.Yellow;
        private String randomSplash = "";
        private Boolean isRandomSplash;
        private Int32 maxSplashWidth;
        private Boolean splashIsBig = false;

        private Boolean useWeb = true;
        private static WebClient web = new WebClient();
        public static String netSplash = "";
        #endregion Splash System

        public override void Init()
        {
            Logger.log("State: " + this.ToString() + " was initialized.", 2);
            Program.game.IsMouseVisible = true;

            #region Use web
            if (useWeb)
            {
                try
                {
                    netSplash = web.DownloadString("http://dl.dropbox.com/u/149155351/netSplash.txt");
                    if (netSplash == "")
                    {
                        useWeb = false;
                    }
                }
                catch (Exception)
                {
                    useWeb = false;
                }
            }
            #endregion Use web

            #region Splashes
            splashes = new String[] 
            { 
            "Beware of ants!",
            "Lotsa Spaghetti!",
            "Stevie is the Dog!",
            "BLACK HOOOOOOOOOLE!!!",
            "It moves...IT MOVES!!!",
            "Baskets are not symmetrical!",
            "They're after your " + System.Environment.NewLine + "11 foot longs!",
            "Get to the helicopter," + System.Environment.NewLine + "Street Fighter!", 
            "Who calls them 'gamepads'?!",
            "Subway. Eat Flesh.",
            "Stop with the clicking!",
            "Comic Sans, not Monospaced!",
            "Let the slapfights begin!",
            "SHAAAAAAAUUUUN!!!",
            "JAAAAASON!",
            "I'M SPEAKING!!!!",
            "Duel of the Fates!",
            "Clash of Heroes!",
            "Codename:  " + System.Environment.NewLine + "Kitty Energy Drink",
            "Currently " + Game1.knownLines + " lines of code!",
            "John Madden!",
            "Stove was here!",
            "Breaaaaaaaaad...!",
            "The Original Yeti is " + System.Environment.NewLine + "out on a picnic!",
            "FIX IT",
            "There's a K in Kombat!!!",
            "Pokémon is one word!",
            "A wild Davidog has appeared!",
            "I know how to spell!",
            "Your food is bad and you " + System.Environment.NewLine + "should feel bad!",
            "Kill the enemy.",
            "KILL THE ENEMY!!!",
            "Baskets look like T.V.s!",
            randomSplash
            };
            if (useWeb)
            {
                currentSplash = netSplash;
            }
            else
            {
                getNewSplash();
            }
            if (currentSplash == randomSplash)
            {
                isRandomSplash = true;
            }
            #endregion

            buttons = new guiButton[] 
            { 
                new guiButton(1, "Play", new Vector2(210, 180)),
                new guiButton(1, "Quit", new Vector2(210, 280)),
                new guiButton(1, "Tutorial", new Vector2(210,230))
            };
        }

        public override void Update()
        {
            // Cycle buttons.
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].Update();
            }

            if (buttons[0].isButtonClicked()) //Play button
            {
                Program.game.sm.SetState(new StateMap1());
            }
            if (buttons[1].isButtonClicked()) //Exit button
            {
                Program.game.Exit();
            }
            if (buttons[2].isButtonClicked()) //Tutorial button
            {
                Program.game.sm.SetState(new StateTutorial());
            }

            #region Websplash
            if (useWeb && (Game1.tickCount % 300 == 0))
            {
                try
                {
                    netSplash = web.DownloadString("http://dl.dropbox.com/u/149155351/netSplash.txt");
                    if (netSplash == "")
                    {
                        getNewSplash();
                        useWeb = false;
                    }
                    currentSplash = netSplash;
                }
                catch (Exception)
                {
                    useWeb = false;
                }
            }
            #endregion Websplash

            // Run the "FIX IT" special splash.df
            if (currentSplash.Contains("FIX IT"))
            {
                if (Game1.tickCount % 15 == 0)
                {
                    if (currentSplash.Length <= 100)
                    {
                        currentSplash += System.Environment.NewLine + "FIX IT";
                    }
                }
            }

            //DEBUG: Disable tutorial.
            buttons[2].active = false;

            #region Splash System
            if (isRandomSplash)
            {
                randomSplash = "RANDOM!: " + r.Next(Int32.MaxValue).ToString();
                currentSplash = randomSplash;
            }

            if (splashMod >= 5)
            {
                splashModDir = 0;
            }
            if (splashMod <= -5)
            {
                splashModDir = 1;
            }
            if (splashModDir == 0)
            {
                splashMod -= 0.2f;
            }
            else
            {
                splashMod += 0.2f;
            }

            if (splashMod < 1)
            {
                fntSplash.Spacing = 0;
            }
            else
            {
                fntSplash.Spacing = Math.Abs(splashMod - 4);
            }
            #endregion Splash System
        }

        public override void LoadContent()
        {
            texTitleCard = Program.game.LoadTexture("gui/titleCard");
            fntSplash = Program.game.LoadFont("fntSplash");
            fntVersion = Program.game.LoadFont("StageTitle");
            // Set the tested maxSplashWidth for the auto rotate system 
            // (tests if the splash is too wide to fit. Not fullproof).
            maxSplashWidth = (int)(fntSplash.MeasureString("AAAAAAAAAAAAAAAAAB").X);

            // Load content for each button.
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].LoadContent();
            }
        }

        public override void UnloadContent()
        {
        }

        public override void Input(KeyboardState thaKeyboard, MouseState thaMouse)
        {
            mouseX = thaMouse.X;
            mouseY = thaMouse.Y;

            // Cycle buttons.
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].Input(thaMouse);
            }

            //DEBUG Get new splash if D and Space are pressed simultaneously.
            if ((thaKeyboard.IsKeyDown(Keys.D)) && (InputManager.IsKeyPressed(Keys.Space, thaKeyboard, thaOldKeyboard)))
            {
                if (StateMenu.netSplash == "Cheats!")
                {
                    getNewSplash();
                }
            }
            
            if ((thaKeyboard.IsKeyDown(Keys.D)) && (InputManager.IsKeyPressed(Keys.E, thaKeyboard, thaOldKeyboard)))
            {
                if (StateMenu.netSplash == "Cheats!")
                {
                    Editor editor = new Editor();
                    editor.Show();
                }
            }

            thaOldKeyboard = thaKeyboard;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            // Draw background.
            spriteBatch.Draw(texTitleCard, new Rectangle(0, 0, Game1.GAME_WIDTH, Game1.GAME_HEIGHT), Color.White);
            spriteBatch.DrawString(fntVersion, "V" + Game1.GAME_VERSION, new Vector2(Game1.GAME_WIDTH - fntVersion.MeasureString("V" + Game1.GAME_VERSION).X, Game1.GAME_HEIGHT - fntVersion.MeasureString("V" + Game1.GAME_VERSION).Y),Color.White);
            // Draw splash.
            if (splashIsBig == true)
            {
                spriteBatch.DrawString(fntSplash, currentSplash, new Vector2(440, 90 + splashMod), splashCol, 0.1f, new Vector2(0, 0), 1, SpriteEffects.None, 0);
            }
            else
            {
                spriteBatch.DrawString(fntSplash, currentSplash, new Vector2(480, 90 + splashMod), splashCol, 0.2f, new Vector2(0, 0), 1, SpriteEffects.None, 0);
            }

            //Cycle buttons.
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].Draw(spriteBatch);
            }
        }

        public override void DeInit()
        {
        }

        private void getNewSplash()
        {
            // Set the current splash to a random splash.
            currentSplash = splashes[r.Next(splashes.Length)];

            if (fntSplash != null) //TODO: fix, make run after LoadContent
            {
                // If splash is too big.
                if ((int)(fntSplash.MeasureString(currentSplash).X) > maxSplashWidth)
                {
                    splashIsBig = true;
                }
            }

            // If the splash is empty, get a new splash.
            if (currentSplash == "")
            {
                getNewSplash();
            }
        }
    }
}
