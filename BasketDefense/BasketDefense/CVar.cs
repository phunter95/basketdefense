﻿//-----------------------------------------//
// Basket Defense | CVar.cs----------------//
// By: Phillip Hunter----------------------//
// Desc: Handles debug variables.----------//
//-----------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BasketDefense
{
    class CVar
    {
        #region CVars
        public static int grid = 0;
        public static int mouseInfo = 0;
        public static int overlays = 0;
        #endregion CVars

        public static void set(ref Int32 var, Int32 val)
        {
            var = val;
        }

        public static void toggleCvar(ref Int32 var)
        {
            if (var == 1)
            {
                var = 0;
            }
            else
            {
                var = 1;
            }
        }
    }
}
