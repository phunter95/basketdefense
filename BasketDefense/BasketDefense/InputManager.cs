﻿//--------------------------------------------------------//
// Basket Defense | InputManager.cs-----------------------//
// By: Phillip Hunter-------------------------------------//
// Desc: Manages certain input functionality.-------------//
//--------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace BasketDefense
{
    public class InputManager
    {
        /// <summary>
        /// Checks if a key is pressed and released.
        /// </summary>
        /// <param name="key">The key to test for.</param>
        /// <param name="newKeyboard">The current keyboard state instance.</param>
        /// <param name="oldKeyboard">The previous keyboard state instance.</param>
        /// <returns></returns>
        public static bool IsKeyPressed(Keys key, KeyboardState newKeyboard, KeyboardState oldKeyboard)
        {
            if ((oldKeyboard.IsKeyUp(key)) && (newKeyboard.IsKeyDown(key)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if the left mouse button is pressed and released.
        /// </summary>
        /// <param name="newMouse">The current mouse state instance.</param>
        /// <param name="oldMouse">The previous mouse state instance.</param>
        /// <returns></returns>
        public static bool IsMouseLeftPressed(MouseState newMouse, MouseState oldMouse)
        {
            if ((oldMouse.LeftButton == ButtonState.Released) && (newMouse.LeftButton == ButtonState.Pressed))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
