//--------------------------------------------------------------------------------//
// Basket Defense | Game1.cs------------------------------------------------------//
// By: Phillip Hunter-------------------------------------------------------------//
// Desc: Runs the state manager and manages global variables.---------------------//
//--------------------------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using BasketDefenseEditor;

namespace BasketDefense
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        // Variables
        #region Game
        public static String GAME_TITLE = "Basket Defense";
        public static String GAME_AUTHOR = "Phillip Hunter";
        public static String GAME_VERSION = "1.0.0";
        public static Int16 knownLines = 2746; // Currently known lines of code, used for a splash.

        private GraphicsDeviceManager graphics;
        public static Color graphicsDeviceClearColor = Color.CornflowerBlue;
        private SpriteBatch spriteBatch;
        public static Int64 tickCount = 0; // Increments each tick.
        public static Int16 GAME_WIDTH = 854;
        public static Int16 GAME_HEIGHT = 600;
        public static ConsoleColor consoleColor = ConsoleColor.Cyan;
        public static Boolean exitFlag = false; // Set to false to close the program.
        private static Boolean inCMD = false; // Is the user entering a console command?

        public StateManager sm = new StateManager();
        #endregion Game

        #region Input
        public static MouseState thaMouse;
        KeyboardState thaKeyboard;
        KeyboardState thaOldKeyboard;
        #endregion Input

        #region Global Textures
        // Enemies
        #region Black Ant
        public static Texture2D texBlackAntL;
        public static Texture2D texBlackAntD;
        public static Texture2D texBlackAntR;
        public static Texture2D texBlackAntU;
        #endregion Black Ant

        #region Army Ant
        public static Texture2D texArmyAntL;
        public static Texture2D texArmyAntD;
        public static Texture2D texArmyAntR;
        public static Texture2D texArmyAntU;
        #endregion Army Ant

        #region Fire Ant
        public static Texture2D texFireAntL;
        public static Texture2D texFireAntD;
        public static Texture2D texFireAntR;
        public static Texture2D texFireAntU;
        #endregion Fire Ant

        // Towers
        #region Test Tower
        public static Texture2D texTestTower;
        public static Texture2D texTestTowerTurret;
        #endregion Test Tower

        #region Basic Ant Shooter Tower
        public static Texture2D texBasicAntShooter;
        public static Texture2D texBasicAntShooterTurret;
        #endregion Basic Ant Shooter Tower

        #region Water Tower
        public static Texture2D texWaterTower;
        public static Texture2D texWaterTowerTurret;
        #endregion Water Tower

        //Projectiles
        #region Projectiles
        public static Texture2D texProj1;
        #endregion Projectiles

        // GUI
        #region GUI
        public static Texture2D texRadius;
        public static Video introVideo;
        #endregion GUI

        // Debug
        #region Debug
        public static Texture2D texError;
        public static Texture2D texWhite;
        public static Texture2D texDCUp;
        public static Texture2D texDCDown;
        public static Texture2D texDCRight;
        public static Texture2D texDCLeft;
        #endregion Debug
        #endregion Global Textures

        #region Audio
        public static SoundEffect audError;
        public static SoundEffect audMenuClick;
        public static SoundEffect audMenuHover;
        public static SoundEffect audTowerPlace;
        public static SoundEffect audTowerFire;
        public static SoundEffect audTowerFireWater;

        public static Single GUI_VOLUME = 1.0f;
        public static Single SFX_VOLUME = 1.0f;
        #endregion Audio

        public Game1()
        {
            Console.ForegroundColor = consoleColor;
            Logger.log(GAME_TITLE + " Version: " + GAME_VERSION + " by " + GAME_AUTHOR + " has started.", 2);
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = GAME_WIDTH;
            graphics.PreferredBackBufferHeight = GAME_HEIGHT;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            this.Window.Title = GAME_TITLE + " Version: " + GAME_VERSION + " by " + GAME_AUTHOR;
            sm.Init();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            #region Global Textures
            // Enemies
            #region Black Ant
            texBlackAntL = Program.game.LoadTexture("enemy/BlackAnt/_BlackAntL");
            texBlackAntR = Program.game.LoadTexture("enemy/BlackAnt/_BlackAntR");
            texBlackAntD = Program.game.LoadTexture("enemy/BlackAnt/_BlackAntD");
            texBlackAntU = Program.game.LoadTexture("enemy/BlackAnt/_BlackAntU");
            #endregion Black Ant

            #region Army Ant
            texArmyAntL = Program.game.LoadTexture("enemy/ArmyAnt/ArmyAntL");
            texArmyAntR = Program.game.LoadTexture("enemy/ArmyAnt/ArmyAntR");
            texArmyAntD = Program.game.LoadTexture("enemy/ArmyAnt/ArmyAntD");
            texArmyAntU = Program.game.LoadTexture("enemy/ArmyAnt/ArmyAntU");
            #endregion Army Ant
            
            #region Fire Ant
            texFireAntL = Program.game.LoadTexture("enemy/FireAnt/FireAntL");
            texFireAntR = Program.game.LoadTexture("enemy/FireAnt/FireAntR");
            texFireAntD = Program.game.LoadTexture("enemy/FireAnt/FireAntD");
            texFireAntU = Program.game.LoadTexture("enemy/FireAnt/FireAntU");
            #endregion Fire Ant

            //Towers
            #region Test Tower
            texTestTower = LoadTexture("tower/TestTower");
            texTestTowerTurret = LoadTexture("tower/TestTowerTurret");
            #endregion Test Tower

            #region Basic Ant Shooter Tower
            texBasicAntShooter = LoadTexture("tower/BasicAntShooter");
            texBasicAntShooterTurret = LoadTexture("tower/BasicAntShooterTurret");
            #endregion Basic Ant Shooter Tower

            #region Basic Ant Shooter Tower
            texWaterTower = LoadTexture("tower/WaterTower");
            texWaterTowerTurret = LoadTexture("tower/WaterTowerTurret");
            #endregion Basic Ant Shooter Tower

            //Projectiles
            #region Projectiles
            texProj1 = LoadTexture("projectiles/projectile1");
            #endregion Projectiles

            //GUI
            #region GUI
            texRadius = LoadTexture("gui/circ");
            try
            {
                introVideo = Content.Load<Video>("textures/gui/introScreen/introVideo");
            }
            catch (Exception)
            {
                Logger.log("Intro video could not be loaded. Using fallback.", 3);
                StateIntroScreen.useFallbackSystem = true;
            }
            #endregion GUI

            //Debug
            #region Debug
            texError = LoadTexture("debug/error");
            texWhite = LoadTexture("debug/white");
            texDCUp = LoadTexture("debug/dChangerU");
            texDCDown = LoadTexture("debug/dChangerD");
            texDCLeft = LoadTexture("debug/dChangerL");
            texDCRight = LoadTexture("debug/dChangerR");
            #endregion Debug
            #endregion Global Textures
            
            #region Audio
            audError = LoadWAV("error");
            audMenuClick = LoadWAV("menuClick");
            audMenuHover = LoadWAV("menuHover");
            audTowerPlace = LoadWAV("towerPlace");
            audTowerFire = LoadWAV("towerFire");
            audTowerFireWater = LoadWAV("towerFireWater");
            #endregion Audio

            sm.LoadContent();
        }

        protected override void UnloadContent()
        {
            sm.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            // Run the tick counter.
            if (tickCount >= (Int64.MaxValue - 10))
            {
                tickCount = 0;
            }
            else
            {
                tickCount++;
            }

            // Exit if exit flag is true.
            if (exitFlag == true)
            {
                this.Exit();
            }

            // Iterate global input.
            Input();

            // Iterate state manager.
            sm.Update();

            base.Update(gameTime);
        }

        public void Input()
        {
            // Get mouse and keyboard states.
            if (this.IsActive)
            {
                thaMouse = Mouse.GetState();
                thaKeyboard = Keyboard.GetState();
            }

            #region Global Input
            // Get console commands.
            if (InputManager.IsKeyPressed(Keys.OemTilde, thaKeyboard, thaOldKeyboard))
            {
                if (!inCMD)
                {
                    Game1.ConsoleCommand();
                }
            }

            //DEBUG: Set state to Map1.
            if (InputManager.IsKeyPressed(Keys.R, thaKeyboard, thaOldKeyboard))
            {
                sm.SetState(new StateMap1());
            }

            // Show info about the mouse position.
            if (CVar.mouseInfo == 1)
            {
                if (tickCount % 50 == 0)
                {
                    Logger.log("Mouse Position: (" + thaMouse.X + "," + thaMouse.Y + ")");
                }
            }

            thaOldKeyboard = Keyboard.GetState();
            #endregion Global Input

            sm.Input(thaKeyboard, thaMouse);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(graphicsDeviceClearColor);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);
            sm.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Returns a loaded Texture2D for the given path.
        /// </summary>
        /// <param name="strTexture">Location of texture. Omit "textures/".</param>
        /// <returns>Texture2D</returns>
        public Texture2D LoadTexture(String strTexture)
        {
            Texture2D tex;
            strTexture = "textures/" + strTexture;
            try
            {
                tex = this.Content.Load<Texture2D>(strTexture);
                Logger.log("The texture " + strTexture + " was loaded successfully.", 2);
            }
            catch (Exception e)
            {
                tex = this.Content.Load<Texture2D>("textures//debug//error");
                Logger.log(e.Message, 3);
                Logger.log("The texture " + strTexture + " could not be loaded successfully.", 2);
            }
            return tex;
        }

        /// <summary>
        /// Returns a loaded SpriteFont for the given path.
        /// </summary>
        /// <param name="strTexture">Location of font. Omit "fonts/".</param>
        /// <returns>SpriteFont</returns>
        public SpriteFont LoadFont(String strFont)
        {
            SpriteFont fnt;
            strFont = "fonts/" + strFont;
            try
            {
                fnt = this.Content.Load<SpriteFont>(strFont);
                Logger.log("The font " + strFont + " was loaded successfully.", 2);
            }
            catch (Exception e)
            {
                fnt = this.Content.Load<SpriteFont>("fonts\\fntError");
                Logger.log(e.Message, 3);
                Logger.log("The font " + strFont + " could not be loaded successfully.", 2);
            }
            return fnt;
        }

        /// <summary>
        /// Returns a loaded SpriteFont for the given path.
        /// </summary>
        /// <param name="strTexture">Location of font. Omit "fonts/".</param>
        /// <returns>SpriteFont</returns>
        public SoundEffect LoadWAV(String strAudio)
        {
            SoundEffect aud;
            strAudio = "audio/" + strAudio;
            try
            {
                aud = this.Content.Load<SoundEffect>(strAudio);
                Logger.log("The WAV sound effect " + strAudio + " was loaded successfully.", 2);
            }
            catch (Exception e)
            {
                aud = this.Content.Load<SoundEffect>("audio\\error");
                Logger.log(e.Message, 3);
                Logger.log("The WAV sound effect " + strAudio + " could not be loaded successfully.", 2);
            }
            return aud;
        }

        /// <summary>
        /// Query the user for a console command.
        /// </summary>
        public static void ConsoleCommand()
        {
            inCMD = true;
            Console.ForegroundColor = System.ConsoleColor.Green;
            Logger.log("");
            Logger.log("Please enter a command.");
            String command = Console.ReadLine().ToUpper(); // Get the text entered by user.

            String output;

            // If the toggle command is entered, then toggle the CVar entered.
            if (command.Contains("/TOGGLE "))
            {
                output = "";
                command = command.Remove(command.IndexOf("/"), 8);
                #region CVar Toggles
                switch (command)
                {
                    case "GRID":
                        CVar.toggleCvar(ref CVar.grid);
                        break;
                    case "MOUSEINFO":
                        CVar.toggleCvar(ref CVar.mouseInfo);
                        break;
                    case "OVERLAYS":
                        CVar.toggleCvar(ref CVar.overlays);
                        break;
                }
                #endregion
                Logger.log("CVar " + command + " toggled.");
            }
            else
            {
                output = GetCommandOutput(command); // Get result of command.
                Logger.log(output);
                ExecCommand(command); //Run command.
            }
            inCMD = false;
            Console.ForegroundColor = consoleColor;
        }

        /// <summary>
        /// Gets the result to a particular command.
        /// </summary>
        /// <param name="command">Command entered by the user.</param>
        /// <returns></returns>
        private static string GetCommandOutput(String command)
        {
            switch (command)
            {
                case "EXIT":
                    return "The game will now exit.";
                case "EDITOR":
                    return "Level Editor Opened.";
                default:
                    return "_";
            }
        }

        /// <summary>
        /// Executes a command entered by the user.
        /// </summary>
        /// <param name="command">Command entered by the user.</param>
        private static void ExecCommand(String command)
        {
            switch (command)
            {
                case "EXIT":
                    exitFlag = true;
                    break;
                case "EDITOR":
                    Editor editor = new Editor();
                    editor.Show();
                    break;
                default:
                    Logger.log("Invalid command entered.");
                    break;
            }
        }
    }
}

