﻿//----------------------------------------------------------------------------------------//
// Basket Defense | DChanger.cs-----------------------------------------------------------//
// By: Phillip Hunter---------------------------------------------------------------------//
// Desc: Game object for changing the direction of Enemy instances when collided with.----//
//----------------------------------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BasketDefense
{
    class DChanger
    {
        public int dir; //0 = Down, 1 = Up, 2 = Left, 3 = Right;
        private Vector2 pos;
        private int width;
        private int height;
        public Rectangle coll;
        private Texture2D texture;

        /// <summary>
        /// Make a direction changer used for Enemy pathing.
        /// </summary>
        /// <param name="dir">0 = Down, 1 = Up, 2 = Left, 3 = Right;</param>
        public DChanger(int dir, int x, int y, int w, int h)
        {
            this.dir = dir;
            this.pos.X = x;
            this.pos.Y = y;
            this.width = w;
            this.height = h;
            switch (this.dir)
            {
                case 0:
                    texture = Game1.texDCDown;
                    break;
                case 1:
                    texture = Game1.texDCUp;
                    break;
                case 2:
                    texture = Game1.texDCLeft;
                    break;
                case 3:
                    texture = Game1.texDCRight;
                    break;
            }
            coll = new Rectangle(x, y, w, h);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, coll, new Color(255, 255, 255, 128));
        }
    }
}
