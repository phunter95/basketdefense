﻿//---------------------------------------------------------------//
// Basket Defense | State.cs-------------------------------------//
// By: Phillip Hunter--------------------------------------------//
// Desc: State class inherited by all game states.---------------//
//---------------------------------------------------------------//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BasketDefense
{
    public abstract class State
    {
        public abstract void Init();
        public abstract void LoadContent();
        public abstract void UnloadContent();
        public abstract void Update();
        public abstract void Input(KeyboardState thaKeyboard, MouseState thaMouse);
        public abstract void Draw(SpriteBatch spriteBatch);
        public abstract void DeInit();
    }
}
